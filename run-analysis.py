#!./sandbox/bin/python

import subprocess
import sys
import os
from dotenv import load_dotenv
import time

load_dotenv(f'{os.getcwd()}/.env')

def run_command(command, lifespan = 0):
    print(f'[sandbox] About to run \'{command}\'')
    p = subprocess.Popen(command, cwd=os.getcwd(), shell=True)
    p.wait()

    print(f'[sandbox] Previous command exited with code {p.returncode}')
    return p.returncode

def shutdown_vm(MACHINE_NAME):
    command = f'vboxmanage controlvm {MACHINE_NAME} acpipowerbutton'
    if run_command(command) == 0:
        wait_till_shutdown = 30
        print(f'[sandbox] Virtual Machine {MACHINE_NAME} received the SHUTDOWN command. Waiting {wait_till_shutdown}s for it to take effect...')
        time.sleep(wait_till_shutdown)

def start_vm(MACHINE_NAME):
    command = f'vboxmanage startvm {MACHINE_NAME}'
    if run_command(command) == 0:
        wait_till_shutdown = 30
        print(f'[sandbox] Virtual Machine {MACHINE_NAME} received the START command. Waiting {wait_till_shutdown}s for it to take effect')
        time.sleep(wait_till_shutdown)

def restore_vm_state(MACHINE_NAME, SNAPSHOT_NAME):
    command = f'vboxmanage snapshot {MACHINE_NAME} restore {SNAPSHOT_NAME}'
    run_command(command)

def run_malware_in_victim_vm(VICTIM_MACHINE_NAME, VICTIM_USER, VICTIM_PASSWORD, RUN_AS_SPC_PATH, session_lifespan):
    command = f'vboxmanage guestcontrol {VICTIM_MACHINE_NAME} --username {VICTIM_USER} --password {VICTIM_PASSWORD} run {RUN_AS_SPC_PATH} /user:{VICTIM_USER} /password:{VICTIM_PASSWORD} --timeout {session_lifespan*60000} /program:Z:\\software.exe'
    run_command(command, session_lifespan)

def download_malware(URL, DESTINATION_FOLDER):
    command = f'wget {URL} -O {DESTINATION_FOLDER+"/software.exe"}'
    run_command(command)

def clean_folder(FOLDER):
    command = f"rm -rf {FOLDER}/*"
    run_command(command)

def restart_vm(MACHINE_NAME, SNAPSHOT_NAME, UPLOAD_FOLDER):
    shutdown_vm(MACHINE_NAME)
    restore_vm_state(MACHINE_NAME, SNAPSHOT_NAME)
    setup_pcap_capture(MACHINE_NAME)
    start_vm(MACHINE_NAME)

def setup_pcap_capture(MACHINE_NAME):
    command = f"rm -rf {os.getcwd()}/lastcapture.pcap"
    run_command(command)
    command = f"vboxmanage modifyvm {MACHINE_NAME} --nictrace1 on --nictracefile1 {os.getcwd()}/results/last-capture.pcap"
    run_command(command)
    run_command("pwd")

def run_network_analysis():
    command = f"tshark -r {os.getcwd()}/results/last-capture.pcap -Y '(not llmnr and not arp and not nbns and not browser and not igmp and not icmp and not tcp) or (tcp and tcp.flags eq 0x0018)' -T fields -e frame.number -e _ws.col.Time -e ip.src -e ip.dst -e frame.len -e _ws.col.Protocol -e _ws.col.Info -E header=y -E separator=, -E quote=d, -E occurrence=f > {os.getcwd()}/results/last-results.csv"
    run_command(command)

def pack_results(SHARED_FOLDER):
    commands = [f'zip -A {os.getcwd()}/results_zip {os.getcwd()}/results/*']
    for command in commands:
        run_command(command)

def main():
    if len(sys.argv) > 2:
        VICTIM_MACHINE_NAME = os.getenv("VICTIM_MACHINE_NAME")
        VICTIM_USER = os.getenv("VICTIM_USER")
        VICTIM_PASSWORD = os.getenv("VICTIM_PASSWORD")
        RUN_AS_SPC_PATH = os.getenv("RUN_AS_SPC_PATH")
        SNAPSHOT_VICTIM_MACHINE = os.getenv("SNAPSHOT_VICTIM_MACHINE")
        UPLOAD_FOLDER = os.getenv("UPLOAD_FOLDER")
        RESULTS_FOLDER = os.getenv("RESULTS_FOLDER")

        clean_folder(f'{os.getcwd()}/{RESULTS_FOLDER}')

        clean_folder(f'{os.getcwd()}/{UPLOAD_FOLDER}')

        print(f'[sandbox] Resetting the victim machine state to its baseline snapshot \'{SNAPSHOT_VICTIM_MACHINE}\'')
        restart_vm(VICTIM_MACHINE_NAME, SNAPSHOT_VICTIM_MACHINE, UPLOAD_FOLDER)

        print(f'[sandbox] Downloading malware...')
        download_malware(sys.argv[1], UPLOAD_FOLDER)
        
        print(f'[sandbox] Executing malware... Lifespan was set to {sys.argv[2]} minutes')
        run_malware_in_victim_vm(VICTIM_MACHINE_NAME, VICTIM_USER, VICTIM_PASSWORD, RUN_AS_SPC_PATH, int(sys.argv[2]))

        print(f'[sandbox] Shutting victim machine down...')
        shutdown_vm(VICTIM_MACHINE_NAME)

        print(f'[sandbox] Analysis finished! The victim machine state was preserved for further investigation, proceed with caution')
        
        print(f'[sandbox] Running network analysis')
        run_network_analysis()

        print(f'[sandbox] Preparing file with results...')
        pack_results(UPLOAD_FOLDER)
    else:
        print(f'[sandbox] Correct use is: {sys.argv[0]} url-to-download-the-malware-from life-span-of-victim-in-minutes')

if __name__ == '__main__':
    main()
