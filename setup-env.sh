#!/bin/bash

rm -rf ./sandbox && \
mkdir ./shared-folder && \
mkdir ./results && \
printf "[setup-env] Creating new python environment...\n" && \
python -m venv ./sandbox && \
source ./sandbox/bin/activate && \
printf "[setup-env] Installing python requirements...\n" && \
python -m pip install -r requirements.txt && \
# printf "[setup-env] Downloading virtual disk image...\n" && \
# gdown --id 1Tx2Nqsi121bANTgQ7s1FNdGtWOG01DR1 --output ./win7.vdi && \
printf "[setup-env] Setting up victim machine...\n" && \
./setup-target-vm.sh && \
printf "[setup-env] You can execute the web API running (in this same foder):\n\t" && \
printf "./launch-api.sh\n" && \
printf "[setup-env] In order to execute an analysis run (in this same foder):\n\t" && \
printf "./run-analysis <url-to-download-the-malware-executable> <lifespan-for-malware-execution-in-minutes>\n" && \
printf "[setup-env] You can also try the web API at 'localhost:8000/docs' (see messages above)\n"
