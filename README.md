# Sandbox Project

To setup your environment run (Linux only):
```
./setup-env.sh
```

This is going to setup the python environment, install all the libraries, download the virtual disk image and set up the virtual machine under virtual box.

The Sandbox has two interfaces, Web API and CLI

## Web API

The web API can be launched by running ```./launch-api.sh```. You can then go to your web browser and visit ```localhost:8000/docs```. The route to be used is quite obvious ("/victim/report"). This "mode" is quite interesting because you can receive the results of the analysis in your email for convenience. You can run multiple requests and they are going to be enqueued for execution (only one analysis at most executing at any given time).

#### Don't execute scripts other than ```setup-env.sh``` or ```run-analysis.py```, the latter serves the CLI. The former sets up the environment and should be executed only once as it sets your vm up. If something goes wrong, make sure to delete all the vm configuration files before trying again as they may end up mangling your installation along the way if left unnatended.

## CLI

The analysis can be executed through the terminal:

```
./run-analysis.py <url-to-download-the-malware-from> <execution-lifespan-in-minutes>
```

In this case the results are going to be made available at:
``results_zip``, a weirdly named file (because google has some rules on scanning files for possible threats).

## Tools
This project assumes that some tools are available in the system, like ``tshark``, ``wget``, ``vboxmanage``(from Virtual Box), ```python``` and several others.

Beyond these tools there are two useful scripts for windows:

- ```provisioning-script.ps1```
- ```registry-reporter.bat```

You can use those to perform more detailed analysis when it comes to file events and registry updates during manual sessions inside the sandbox (the shared folder between the host and the guest is bidirectional to make things easier, be careful)
