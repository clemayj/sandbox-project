param(
    [string]$url,
    [int]$hangTimeInSeconds = 30,
    [string]$logFilePath = "\\VBOXSVR\shared\Log.txt"
)

# Download content from the specified URL
try {
    # Set-ExecutionPolicy Bypass -scope Process -Force
    $webClient = New-Object System.Net.WebClient
    $filePath = "C:\Users\Sysuser\Downloads\virus.exe"
    $webClient.DownloadFile($url, $filePath)
    Write-Output "Download successful"
    Start-Process -FilePath $filePath -Wait
    Write-Output "File executed: $filePath"
}
catch {
    Write-Error "Failed to download content from the URL"
}

# Hang for the specified time
Write-Output "Hanging for $hangTimeInSeconds seconds..."
Start-Sleep -Seconds $hangTimeInSeconds

# Log events from the Windows Event Logger to a file
try {
    $events = Get-WinEvent -LogName "Application" -After (Get-Date).AddDays(-1) -ErrorAction Stop
    $events | Format-Table -AutoSize | Out-File -FilePath $logFilePath -Append
    Write-Output "Events logged to $logFilePath"
}
catch {
    Write-Error "Failed to retrieve or log events"
}
