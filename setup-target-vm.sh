#!/bin/bash

# ARGC=$#

# if [ $ARGC -gt 2 ]; then

    VM_NAME="demo-win7"
    SHARED_FOLDER_HOST="shared-folder"
    VDI_FILE_PATH="./win7.vdi"

    set -x

    # BASIC SETUP
    # Create the vm
    vboxmanage createvm --name $VM_NAME --ostype "Windows7_64" --register --basefolder $VM_NAME

    # set memory to 2G, video memory to 128MB
    VBoxManage modifyvm $VM_NAME --ioapic on --memory 2096 --vram 128

    # STORAGE SETUP
    # create a storage controller (sata type with advanced host controller interface)
    vboxmanage storagectl $VM_NAME --name "SATA Controller" --add sata --controller IntelAhci

    # attach the virtual disk image we have to the adapter we just created as an hdd storage device at port 0
    vboxmanage storageattach $VM_NAME --storagectl "SATA Controller" --device 0 --port 0 --type hdd --medium $VDI_FILE_PATH

    # DISPLAY SETUP
    # set display config
    vboxmanage modifyvm $VM_NAME --vrde on
    vboxmanage modifyvm $VM_NAME --vrdemulticon on --vrdeport 10001

    # NETWORK SETUP
    # setup internet adapter
    vboxmanage modifyvm $VM_NAME --nic1 nat --nictrace1 on

    # set some config to allow for the machine to be booted
    # vboxheadless --startvm $VM_NAME

    # power the machine down gracefully (must test if it forces the machine to shutdown arbitrarily)
    # vboxmanage controlvm $VM_NAME acpipowerbutton


    # restore a specific snapshot
    # vboxmanage snapshot $VM_NAME restore 322d5674-f71a-4652-af35-9b1f59d0ba69

    # list all the snapshots associated with the vm
    # vboxmanage snapshot $VM_NAME list

    # OPTICAL DISC DRIVE SETUP
    # create and attach an optical disc drive in order to have the shared folder built
    vboxmanage storagectl $VM_NAME --name "IDE" --add ide --controller PIIX4
    vboxmanage storageattach $VM_NAME --storagectl "IDE" --port 1 --device 1 --type dvddrive --medium emptydrive

    # SHARED FOLDER SETUP
    # create a shared folder
    vboxmanage sharedfolder add $VM_NAME --name "shared" --hostpath $SHARED_FOLDER_HOST --automount --auto-mount-point "Z:\\"

    # VIRTUAL MACHINE STARTUP
    # effectively boots the machine up
    # vboxmanage startvm $VM_NAME

    # take a snapshot and set its name as "text-file"
    vboxmanage snapshot $VM_NAME take stable-win7-victim
# else
#     echo "[warning] - correct use is: $0 <vm-name> <shared-folder> <vdi-file-path>"
# fi
