from typing import Union

from fastapi import FastAPI, HTTPException, BackgroundTasks
from time import sleep
import subprocess
import smtplib, ssl
from os import listdir, getcwd
import os
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from threading import Lock

app = FastAPI()

mutex = Lock()

@app.get("/")
def read_root():
    return {"Welcome to our Sandbox API!"}


def run_analysis_and_report(url, lifespan, email_address):
    with mutex:
        command = f'./run-analysis.py {url} {lifespan}'
        print(os.getcwd())
        p = subprocess.Popen(command.split())
        p.wait()
        
        port = 465  # For SSL
        smtp_server = "smtp.gmail.com"
        sender_email = "cve.202327043@gmail.com"
        password = "bwqv tcyq zxxe iulm"

        context = ssl.create_default_context()
        with smtplib.SMTP_SSL(smtp_server, port, context=context) as server:
            server.login(sender_email, password)

            msg = MIMEMultipart()
            msg['Subject'] = f'[TheSandbox] Results for {url} (lifespan: {lifespan} minutes)'
            

            if(p.returncode == 0):
                text = MIMEText(f"Hi friend,\n\nThe report of network activity for the malware at {url} (executed for at most {lifespan} minutes) is in the attachments (be careful).\n\nAll the very best,\nThe SandBox")
                msg.attach(text)
                with open("./results_zip", "rb") as file:
                    part = MIMEApplication(
                        file.read(),
                        Name="report"
                    )
                part['Content-Disposition'] = 'attachment; filename="%s"' % "results_zip"
                msg.attach(part)

            else:
                text = MIMEText(f"Hi friend,\n\nThe report of network activity for the malware at {url} (meant to be executed for at most {lifespan} minutes) could not be generated, it's likely that something broke along the way!\n\nYou should check your setup, and maybe run the malware manually (don't forget your gloves :P).\n\nAll the very best,\nThe SandBox")
                msg.attach(text)

            server.sendmail(sender_email, email_address, msg.as_string())
            print(f"[sandbox] Message sent to {email_address}")
        


@app.post("/victim/report")
def victim_report(
    url: str, email: str, lifeTime: int, background_tasks: BackgroundTasks
):
    background_tasks.add_task(run_analysis_and_report, url, lifeTime, email)

    return f"The analysis over the piece of malware at {url} has been triggered and is meant to last at most {lifeTime} minutes. Results are going to be sent to {email} as soon as they are available. See ya..."
